# Introduction

This app will list Contributors for a given Github Organization (e.g.: https://github.com/vuejs). Sub-pages will show Contributor or Repository details.

# Installation

## Prerequirements

We need to have npm (node.js) installed to get the app running on local machine.

## Getting Started

1. Clone
2. Run `npm install`
4. Run `npm run serve`
5. Access app at `http://localhost:8080/`
6. Provide Github Organization url and your Github Personal API token (needed to perform more than 50 calls to API - please create new one for this app at https://github.com/settings/tokens)

# TODO

## Improvements
- [ ] Integrate Vuex for better state management
- [ ] Add input fields for Github repository and Token + impl func for that
- [ ] Add API data to the cache
- [ ] Add responsiveness
- [ ] List more than 100 contributors (currently constraint by API pagination)
- [ ] Impl total number of contributions

## Fixes
- [ ] Fix `Unexpected side effect in "sortedContributors" computed property`
