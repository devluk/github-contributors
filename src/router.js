import Vue from 'vue'
import Router from 'vue-router'
import Contributors from './views/Contributors.vue'
import Contributor from './views/Contributor.vue'
import Repository from './views/Repository.vue'

Vue.use(Router)

export default new Router({

  routes: [
    {
      path: '/', redirect: '/contributors'
    },
    {
      path: '/contributors',
      name: 'contributors',
      component: Contributors
    },
    {
      path: '/contributor/:login',
      name: 'contributor',
      component: Contributor,
      props: true
    },
    {
      path: '/repository/:owner/:repo',
      name: 'repository',
      component: Repository,
      props: true
    }
  ]
})
